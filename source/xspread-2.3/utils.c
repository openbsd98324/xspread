
#include "config.h"
#include <math.h>

#ifdef RINT
/**     round-to-even, also known as ``banker's rounding''.
	With round-to-even, a number exactly halfway between two values is
	rounded to whichever is even; e.g. rnd(0.5)=0, rnd(1.5)=2,
	rnd(2.5)=2, rnd(3.5)=4.  This is the default rounding mode for
	IEEE floating point, for good reason: it has better numeric
	properties.  For example, if X+Y is an integer,
	then X+Y = rnd(X)+rnd(Y) with round-to-even,
	but not always with sc's rounding (which is
	round-to-positive-infinity).  I ran into this problem when trying to
	split interest in an account to two people fairly.
**/
double 
rint(d)
double d;
{
	/* as sent */
	double fl = floor(d),  fr = d - fl;

	return ((fr < 0.5) || ((fr == 0.5) && (fl == floor(fl/2) * 2)))
	        ? fl : ceil(d);
}
#endif


/*
 * pow10 calculates the power function of base 10
 */

#ifndef linux
double pow10(p)
#else
double Pow10(p)
   double p;
#endif
{
   double q;

   p = floor(p);
   if (p >= 0) {
      for (q = 1; p > 0; --p)
         q = q * 10;
   }
   else {
      p = -p;
      for (q = 1; p > 0; --p)
         q = q / 10;
   }
   return q;
}

