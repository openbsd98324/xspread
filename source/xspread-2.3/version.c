#include "config.h"

/*  At the moment XSpread and SC are two different programs
 *  So the XSpread version number is still less than then SC version
 */

char *version = " XSpread v2.3   Based on Sc 6.21";

/*
 * CODE REVISION NUMBER:
 *
 * The part after the first colon, except the last char, appears on the screen.
 */

char *rev = "$Revision: 6.21 alpha5 $";

print_help()
{
#if defined(CASEINSENSITIVECMDLINE)
   printf(
   "xspread [-h] [-c] [-m] [-n] [-x] [-fn font] [filename]\n\n");
#else	/* Peter Doemel, 11-Feb-1993 */
   printf(
   "xspread [-h] [-c] [-m] [-n] [-x] [-N] [-C] [-R] [-fn font] [filename]\n\n");
#endif

   printf(
   "         -c:  Column order recalculation.           Default: row order.\n");
   printf(
   "         -m:  Manual recalculation.                 Default: automatic.\n");
   printf(
   "         -n:  Standard Data entry mode.             Default: numeric.\n");
/* printf(
   "         -r:  Row order recalculation.              Default: row order.\n");
*/ /* commented by Peter Doemel, 11-Feb-1993 */
   printf(
   "         -x:  File read/write use encryption.       Default: off.\n");
#if !defined(CASEINSENSITIVECMDLINE)
   printf(
   "         -N:  Don't show cursor.                    Default: show.\n");
   printf(
   "         -C:  Action on Newline: column (go down).  Default: none.\n");
   printf(
   "         -R:  Action on Newline: row (go right).    Default: none.\n");
#endif
   printf(
   "        -fn:  Change the font.\n\n");
   printf(
   "         -h:  Command line help.\n");
   printf(
   "Options can be changed through ctrl-t toggle commands.\n\n");
} 

