\contentsline {section}{Introduction}{1}
\contentsline {section}{Using the Worksheet}{4}
\contentsline {subsection}{Worksheet Structure}{4}
\contentsline {subsection}{Navigating the Worksheet}{4}
\contentsline {subsection}{Cell Entry and Editing}{5}
\contentsline {subsection}{Formulas, Cell Expressions, and Functions}{6}
\contentsline {subsection}{Toggle Commands}{8}
\contentsline {subsection}{Miscellaneous Commands}{9}
\contentsline {section}{Alphabetical Command Reference}{10}
\contentsline {subsection}{Column/Row Commands}{10}
\contentsline {subsection}{File Commands}{13}
\contentsline {subsection}{Graph Commands}{16}
\contentsline {subsection}{Matrix Commands}{24}
\contentsline {subsection}{Option Commands}{26}
\contentsline {subsection}{Range Commands}{29}
\contentsline {section}{Function Reference}{33}
\contentsline {subsection}{Argument types}{33}
\contentsline {subsection}{Functions by type}{33}
\contentsline {subsection}{Alphabetical Function Reference}{34}
