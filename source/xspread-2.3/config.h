
/*  The default font to use */
#define SC_FONT "9x15"

#ifdef __hpux
#define SYSV2
#ifndef hpux
#define hpux
#endif
#else
#define BSD43
#endif
/* #define SYSV */

/*  Define this if you want backups saved of the files */
#define DOBACKUPS

/*  The name to save the backup worksheet to  */
#define SAVENAME "SC.SAVE"

/*  Define this if you system doesn't have rint in the math lib. */
#if defined(__convex__)
#define RINT
#endif

/*  Define CRYPT_PATH if you want crypt support compiled in */
#define CRYPT_PATH "/bin/crypt"

/*  If you want to run "des" instead of "crypt" for crypting. */
/* #define DES */

/*  The regular expression handlers
 *  RE_COMP for system re_comp/re_exec()   (BSD, SUN)
 *  REGCMP for standard system regcmp/regex handlers (SVR3/4)
 *  REGCOMP for spencer lib use
 *  REG_COMP for POSIX.2 FIPS 
 */

/*  386BSD has patches to use RE_COMP, but some people may not have them */
#if defined(__386BSD__) || defined(__hpux)
#define REGCOMP
#else

#if defined(systemvthing)
#define REGCMP
#else

/* defined(mips) || defined(sun) || defined(__convex__) || defined(__osf__) */
#define RE_COMP

#endif
#endif

/*  Define SIGVOID if you signal routine returns a void */
#define SIGVOID

/*  The default file viewer */
#define DFLT_PAGER "/usr/local/bin/less"

/*  Define doing x if you want this is be xspread don't define it if 
 *  you want this to be SC. Currently you MUST define this
 */
#define DOINGX

/*  Some yuck I found where it turned abs() into some inline routine?? */
#if defined(convex)
#define __NO_INLINE
#endif

/*
#if (defined(BSD42) || defined(BSD43)) && !defined(strrchr)
#define strrchr rindex
#endif

#if (defined(BSD42) || defined(BSD43)) && !defined(strchr)
#define strchr index
#endif
*/

